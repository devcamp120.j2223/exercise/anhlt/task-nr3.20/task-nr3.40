//  Khai báo thư viện express
const express = require('express');

// Import
const { createPost, getAllPosts, getPostById, updatePostById, deletePostById, getPostOfUser } = require('../controller/usercontroller');


//Tạo router
const userRouter = express.Router();

//CREATE A USER
userRouter.post('/post', createPost);


//GET ALL USER
userRouter.get('/post', getAllPosts);


//GET A USER
userRouter.get('/post/:postId', getPostById)


//UPDATE A USER
userRouter.put('/post/:postId', updatePostById)


//DELETE A USER
userRouter.delete('/post/:postId', deletePostById)

module.exports = userRouter