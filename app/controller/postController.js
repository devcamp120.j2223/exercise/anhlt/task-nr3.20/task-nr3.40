//import post model
const { default: mongoose, Schema, Mongoose } = require('mongoose');
const postModel = require('../models/postModel');
const createPost = (request, response) => {
    //B1: thu thập dữ liệu
    let body = request.body;

    //B2: kiểm tra dữ post
    if (!body.userId || !body.title || !body.body) {
        response.status(400).json({
            message: "required!"
        })
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        let post = {
            _id: mongoose.Types.ObjectId(),
            userId: body.userId,
            title: body.title,
            body: body.body,
        }

        postModel.create(post, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(201).json({
                    data
                })
            }
        });
    }
};
//get all Posts
const getAllPosts = (request, response) => {
    //B1. thu thập dữ liệu 
    let userId = request.query.userId
    //B2. kiểm tra dữ liệu (bỏ qua)
    //B3. thực hiện thao tác dữ liệu
    let condition = {};
    if (userId){
        condition.userId = userId;
    }
    postModel.find(condition,(error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(200).json({
                data
            })
        }
    })
};
//get a post by id
const getPostById = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let id = request.params.postid;

    //B2. kiểm tra dữ liệu 
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //B3. thực hiện thao tác dữ liệu
        postModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};
//update a post by id
const updatePostById = (request, response) => {
    //B1. Thu thập dữ liệu
    let id = request.params.postid;
    let body = request.body;

    //B2. Kiểm tra dữ liêu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } if (!body.userId || !body.title || !body.body) {
        response.status(400).json({
            message: " required!"
        })
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        let post = {
            userId: body.userId,
            title: body.title,
            body: body.body,
        }
        postModel.findByIdAndUpdate(id, post, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        });
    }
};
//delete a post by id
const deletePostById = (request, response) => {
    //B1. Thu thập dữ liệu
    let id = request.params.postid;

    //B2. Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "id is invalid!"
        })
    } else {
        //B3: thực hiện các thao tác nghiệp vụ
        postModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        })
    }
};
const getPostOfUser = (request, response) => {
    //B1. thu thập dữ liệu (bỏ qua)
    let userId = request.params.userid;
    console.log(userId);
    //B2. kiểm tra dữ liệu 
    let condition = {};
    if (userId) {
        condition.userId = userId;
    }
    //B3. thực hiện thao tác dữ liệu
        postModel.find(condition, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
};
//export các hàm ra thành module
module.exports = { createPost, getAllPosts, getPostById, updatePostById, deletePostById, getPostOfUser }