// Khai báo mongoose
const mongoose = require('mongoose');

// IMPORT prize model  vào controller
const userModel = require('../models/userModel');

// CREATE A PRIZE
const createUser = (request, response) => {

    //B1: thu thập dữ liệu
    let bodyRequest = request.body;

    //B2: Điều kiện
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "name is required"
        })
    }

    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "username is required"
        })
    }
    if (!bodyRequest.address) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "address is required"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let createuser = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company : bodyRequest.company
    }
    userModel.create(createuser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create user success",
                data: data
            })
        }
    })
}


// LẤY TẤT CẢ USER
const getAlluser = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all user success",
                data: data
            })
        }
    })
}

// LẤY USER THEO ID
const getUserById = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2 : Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    // B3: Thao tắc với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user by id success",
                data: data
            })
        }
    })
}


//UPDATE A PRIZE
const updateUserById = (request, response) => {
    // B1: Thu thập dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "User ID is not valid"
        })
    }

    //B3: Thao tác với cơ sở dữ liệu
    let updateUser = {
        name: bodyRequest.name,
        username: bodyRequest.username,
        email: bodyRequest.email,
        address: bodyRequest.address,
        phone: bodyRequest.phone,
        website: bodyRequest.website,
        company : bodyRequest.company
    }
    userModel.findByIdAndUpdate(userId, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update User success",
                data: data
            })
        }
    })
}


// DELETE A PRIZE
const deleteUserById = (request, response) => {
    // B1: thu thập dữ liệu
    let userId = request.params.userId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad request",
            message: "User Id is not valid"
        })
    }

    //B3: Thao tắc với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
            })
        }
    })
}

// EXPORT
module.exports = { createUser, getAlluser, getUserById, updateUserById, deleteUserById }